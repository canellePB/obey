var Webgl = (function(){

    var objects=[];
    var sphereMeshes=[];
    var materialsLines=[];
    var materialsSquare=[];
    var materialsTriangle=[];
    var materialsHexagone=[];
    var allMaterials = [];
    var moveSlowly= false;
    var length =300;
    var intersected = null;
    var radius = 30;
    var prevIndex;
    var currentIndex;

    function Webgl(width, height){

        this.container = document.getElementById( 'container' );
        this.scene = new THREE.Scene();
        
        this.camera = new THREE.PerspectiveCamera(50, width / height, 1, 10000);
        this.camera.position.z = 3000;

        this.renderer = new THREE.WebGLRenderer({antialias:true});
        this.renderer.setSize(width, height);
        this.renderer.setClearColor(0x000000);
     
        container.innerHTML = "";
        this.container.appendChild( this.renderer.domElement );

        this.vector = new THREE.Vector3();
      
        currentIndex = 1;
        var arrayColors = arraysColors[0];

        flag=true;

        for(var i=0;i<arrayColors.length;i++)
        {
            material = new THREE.LineBasicMaterial( { color: arrayColors[i], name:"mat"+i});
            materialsLines.push(material);
        }
        for(var i=0;i<arrayColors.length;i++)
        {
            material = new THREE.LineBasicMaterial( { color: arrayColors[i], name:"mat"+i});
            materialsSquare.push(material);
        }
        for(var i=0;i<arrayColors.length;i++)
        {
            material = new THREE.LineBasicMaterial( { color: arrayColors[i], name:"mat"+i});
            materialsTriangle.push(material);
        }
        for(var i=0;i<arrayColors.length;i++)
        {
            material = new THREE.LineBasicMaterial( { color: arrayColors[i], name:"mat"+i});
            materialsHexagone.push(material);

        }

        allMaterials = [materialsHexagone, materialsLines, materialsTriangle, materialsSquare];


        this.controls = new THREE.OrbitControls( this.camera,this.renderer.domElement );
        this.initAllShapes();
        window.requestAnimationFrame(this.render.bind(this));


    }
    Webgl.prototype.initAllShapes=function(indexGeometry){


        for ( var i = 0; i < length; i ++) {


            sphereObject = new Sphere(radius);


            hexagone = new Hexagone(radius, materialsHexagone[(Math.floor(Math.random() * 5 ))], 6);
            hexagone.material.transparent=true;
            hexagone.material.opacity=0;
            
            line = new Hexagone(radius, materialsLines[(Math.floor(Math.random() * 5 ))], 2);
            line.visible=false;
            line.material.opacity = 0;
            line.material.transparent = true;

            triangle = new Hexagone(radius, materialsTriangle[(Math.floor(Math.random() * 5 ))], 3);
            triangle.visible=false;
            triangle.material.opacity = 0;
            triangle.material.transparent = true;

            square = new Hexagone(radius, materialsSquare[(Math.floor(Math.random() * 5 ))], 4);
            square.visible=false;
            square.material.opacity = 0;
            square.material.transparent = true;

            switch(hexagone.material.name)
            {
                case "mat0":
                   sphereObject.position.x=-200
                break;
                case "mat1":
                    sphereObject.position.x=-100
                break;
                case "mat2":
                    sphereObject.position.x=-0
                break;
                case "mat3":
                    sphereObject.position.x=100
                break;
                case "mat4":
                    sphereObject.position.x=200
                break;

            }
            sphereObject.add( hexagone );
            sphereObject.add( line );
            sphereObject.add( triangle );
            sphereObject.add( square );
            this.scene.add( sphereObject );

            // this.vector.copy( sphereObject.position ).multiplyScalar( 2 );
            // sphereObject.lookAt( this.vector );
            sphereMeshes.push(sphereObject.mesh);
            objects.push(sphereObject);
        }

        TweenLite.to(materialsHexagone[0], 0.5, {opacity:1, delay:1,onCompleteScope:this,onComplete:function(){
            TweenLite.to(materialsHexagone[1], 0.5, {opacity:1, onCompleteScope:this,onComplete:function(){
                TweenLite.to(materialsHexagone[2], 0.5, {opacity:1, onCompleteScope:this,onComplete:function(){

                    TweenLite.to(materialsHexagone[3], 0.5, {opacity:1, onCompleteScope:this,onComplete:function(){

                        TweenLite.to(materialsHexagone[4], 0.5, {opacity:1, onCompleteScope:this, onComplete:function(){

                            this.displaySphere(Circ.easeOut, 3,true);
                          
                        }});
                     }});
                }});

            }});
        }});
       
    }
    Webgl.prototype.toggleGeometry=function(indexGeometry){

        prevIndex = currentIndex;
        currentIndex = indexGeometry;

        that=this;
        setTimeout(this.fadeGeometry.bind(this), 500);

        for ( var i = 1; i < objects.length; i ++) {

            obj = objects[i];
            
            TweenLite.to(obj.rotation, 3, {x:obj.rotation.x+30,y:obj.rotation.y+30,z:obj.rotation.z+10, Power4:Quad.easeInOut});
           

        }


    };




    Webgl.prototype.fadeGeometry=function(){

        flag=true;

        for ( var i = 0; i < objects.length; i ++) {

            that=this;
            obj = objects[i];   
            
            obj.children[currentIndex].visible=true;

            TweenLite.to(obj.children[currentIndex].material, 3, {opacity:1});

            TweenLite.to(obj.children[prevIndex].material, 2, {opacity:0,onCompleteScope:this, 
                onComplete:function()
                {
                    if(flag)
                    {
                        this.hideGeometry();
                        flag=false;
                    }
                    
                }
            });
      

            
        }

        

    };
    Webgl.prototype.hideGeometry=function(){

    

        for ( var i = 0; i < objects.length; i ++) 
        {
            objects[i].children[prevIndex].visible=false;
        }

    };
    Webgl.prototype.displaySphere=function(easing,duration,flag){



        this.vector = new THREE.Vector3();

         for ( var i = 0; i < objects.length; i ++) {

      
            var phi = Math.acos( -1 + ( 2 * i ) / length );
            var theta = Math.sqrt( length * Math.PI ) * phi;


            TweenLite.to(objects[i].position, duration, {x:800 * Math.cos( theta ) * Math.sin( phi ), y:800 * Math.sin( theta ) * Math.sin( phi ), z:800 * Math.cos( phi ), ease:easing} );


            this.vector.copy( objects[i].position ).multiplyScalar( 2 );
            objects[i].lookAt( this.vector );

        }
        moveSlowly = false;
        if(flag)
        {
            
            TweenLite.to(this.camera.position, duration+1, {z:3000,ease:Quart.easeOut, onCompleteScope:this, onComplete:function(){
                $('#tools').animate({
                    opacity:1
                },'slow');
                this.controls.autoRotate=true;
            }});
        }
        // TweenLite.to(this.camera.position, 3, {x:0,y:0,z:3500,ease:Back.easeInOut});
    };

    Webgl.prototype.displayHelix=function(){

        console.log('displayHelix');


        this.vector = new THREE.Vector3();
        var phi = 0.175 + Math.PI;

         for ( var i = 0; i < objects.length; i ++) {


            phi+=0.1;


            TweenLite.to(objects[i].position, 3, {x:500 * Math.sin( phi ), y:- ( i * 8 ) + 800, z:500 * Math.cos( phi ), ease:Circ.easeInOut});


            this.vector.x = objects[i].position.x * 2;
            this.vector.y = objects[i].position.y;
            this.vector.z = objects[i].position.z * 2;

            this.vector.copy( objects[i].position ).multiplyScalar( 2 );
            objects[i].lookAt( this.vector );

        }
        moveSlowly = false;

    };

    Webgl.prototype.toggleColor = function(arrayColors){

        var indexArray = parseInt(arrayColors.substring(6))-1;
        var arrayColors = arraysColors[indexArray];

        currentArrayMat =allMaterials[currentIndex-1];


        for(var i=0;i<arrayColors.length;i++)
        {   
            mat = currentArrayMat[i];
            prevColor = mat.color;
            newColor = new THREE.Color(arrayColors[i]);
            

            TweenLite.to(prevColor, 1, {r:newColor.r,g:newColor.g,b:newColor.b, ease:Back.easeIn, onUpdate:function(){
                mat.color = prevColor;

            }});

            for(var j=0; j<allMaterials.length; j++)
            {

                if(j != currentIndex-1)
                {
                    arrayMat = allMaterials[j];
                    mat=arrayMat[i];
                    mat.color=prevColor;

                }
            }




        }

    };




    Webgl.prototype.resize = function(width, height) {
        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(width, height);
    };

    Webgl.prototype.explode = function(){

        for ( var i = 0; i < objects.length; i ++) {

            TweenLite.to(objects[i].position, 2.5, {x:(Math.random()*3000)-1500,y:(Math.random()*3000)-1500, z:(Math.random()*3000)-1500, ease:Circ.easeInOut});
            moveSlowly = true;
            objects[i].directionX = (Math.random() * 2 )-1;
            objects[i].directionY = (Math.random() * 2 )-1;
            objects[i].directionZ = (Math.random() * 2 )-1;


        }
        // TweenLite.to(this.camera.position, 3, {x:-1500,y:1500,z:-1200,ease:Back.easeInOut});
        $('.shapes img').removeClass('selected');

    };

    Webgl.prototype.displayRose=function(event){

        mat0=[];
        mat1=[];
        mat2=[];
        mat3=[];
        mat4=[];

        for ( var i = 0; i < objects.length; i ++) {

           
            switch(objects[i].children[currentIndex].material.name)
            {
                case "mat0":
                   mat0.push(objects[i])
                break;
                case "mat1":
                    mat1.push(objects[i])
                break;
                case "mat2":
                    mat2.push(objects[i])
                break;
                case "mat3":
                    mat3.push(objects[i])
                break;
                case "mat4":
                    mat4.push(objects[i]);
                break;

            }


        }

        for ( var j = 0; j < mat0.length; j ++) {

            posX = 800 * Math.sin(2 * Math.PI * j / mat0.length);
            posZ = 800 * Math.cos(2 * Math.PI * j / mat0.length); 
    
            TweenLite.to(mat0[j].position, 3, {x:posX,y:0, z:posZ, ease:Quart.easeInOut});

        }

        for ( var j = 0; j < mat1.length; j ++) {

            posZ = -800 * Math.cos(2 * Math.PI * j / mat1.length);
            posY = 800 * Math.sin(2 * Math.PI * j / mat1.length); 
    
            TweenLite.to(mat1[j].position, 3, {x:0,y:posY, z:posZ, ease:Quart.easeInOut});

        }
        for ( var j = 0; j < mat2.length; j ++) {

            posX = -800 * Math.cos(2 * Math.PI * j / mat2.length);
            posY = 800 * Math.sin(2 * Math.PI * j / mat2.length); 
    
            TweenLite.to(mat2[j].position, 3, {x:posX,y:posY, z:0, ease:Quart.easeInOut});

        }
        for ( var j = 0; j < mat3.length; j ++) {

            posX = 1000 * Math.cos(2 * Math.PI * j / mat3.length);
            posY = 1500 * Math.sin(2 * Math.PI * j / mat3.length); 
            posZ = 1000 * Math.cos(2 * Math.PI * j / mat3.length);
    
            TweenLite.to(mat3[j].position, 3, {x:posX/2,y:posY/2, z:posZ/2, ease:Quart.easeInOut});

        }
        for ( var j = 0; j < mat4.length; j ++) {

            posX = -1000 * Math.cos(2 * Math.PI * j / mat4.length);
            posY = 1500 * Math.sin(2 * Math.PI * j / mat4.length); 
            posZ = 1000 * Math.cos(2 * Math.PI * j / mat4.length);
    
            TweenLite.to(mat4[j].position, 3, {x:posX/2,y:posY/2, z:posZ/2, ease:Quart.easeInOut});

        }

        that = this;
        // TweenLite.to(this.camera.position, 3, {x:800,y:1500,z:2000,ease:Back.easeInOut});
        // TweenLite.to(this.camera.position, 2, {x:-200,y:-5000,z:50,ease:Quart.easeInOut, onComplete:function(){
        //    TweenLite.to(that.camera.position, 2, {x:150,y:-2600,z:780,ease:Quart.easeOut});
        // }});



        
        moveSlowly = false;

    };



    Webgl.prototype.displayRings=function(event){

        mat0=[];
        mat1=[];
        mat2=[];
        mat3=[];
        mat4=[];

        for ( var i = 0; i < objects.length; i ++) {

           
            switch(objects[i].children[currentIndex].material.name)
            {
                case "mat0":
                   mat0.push(objects[i])
                break;
                case "mat1":
                    mat1.push(objects[i])
                break;
                case "mat2":
                    mat2.push(objects[i])
                break;
                case "mat3":
                    mat3.push(objects[i])
                break;
                case "mat4":
                    mat4.push(objects[i]);
                break;

            }


        }

        for ( var j = 0; j < mat0.length; j ++) {

            posX = 800 * Math.cos(2 * Math.PI * j / mat0.length);
            posY = 800 * Math.sin(2 * Math.PI * j / mat0.length); 
    
            TweenLite.to(mat0[j].position, 3.5, {x:posX,y:posY, z:0, ease:Quart.easeInOut});

        }

        for ( var j = 0; j < mat1.length; j ++) {

            posX = -800 * Math.cos(2 * Math.PI * j / mat1.length);
            posY = 800 * Math.sin(2 * Math.PI * j / mat1.length); 
    
            TweenLite.to(mat1[j].position, 3.5, {x:posX,y:posY, z:300, ease:Quart.easeInOut});

        }
        for ( var j = 0; j < mat2.length; j ++) {

            posX = -800 * Math.cos(2 * Math.PI * j / mat2.length);
            posY = 800 * Math.sin(2 * Math.PI * j / mat2.length); 
    
            TweenLite.to(mat2[j].position, 3.5, {x:posX,y:posY, z:-300, ease:Quart.easeInOut});

        }
        for ( var j = 0; j < mat3.length; j ++) {

            posX = -800 * Math.cos(2 * Math.PI * j / mat3.length);
            posY = 800 * Math.sin(2 * Math.PI * j / mat3.length); 
    
            TweenLite.to(mat3[j].position, 3.5, {x:posX,y:posY, z:600, ease:Quart.easeInOut});

        }
        for ( var j = 0; j < mat4.length; j ++) {

            posX = -800 * Math.cos(2 * Math.PI * j / mat4.length);
            posY = 800 * Math.sin(2 * Math.PI * j / mat4.length); 
    
            TweenLite.to(mat4[j].position, 3.5, {x:posX,y:posY, z:-600, ease:Quart.easeInOut});

        }

        that = this;
        // TweenLite.to(this.camera.position, 3, {x:800,y:1500,z:2000,ease:Back.easeInOut});
        // TweenLite.to(this.camera.position, 2, {x:-200,y:-5000,z:50,ease:Quart.easeInOut, onComplete:function(){
        //    TweenLite.to(that.camera.position, 2, {x:150,y:-2600,z:780,ease:Quart.easeOut});
        // }});



        
        moveSlowly = false;

    };

    Webgl.prototype.render = function() {

        window.requestAnimationFrame(this.render.bind(this));

    
        this.controls.update();
        this.renderer.render(this.scene, this.camera);

        for(var i=0; i<objects.length;i++)
        {
            objects[i].rotation.x += 0.01;
            objects[i].rotation.y += 0.01;
            objects[i].rotation.z += 0.01;
            // objects[i].children[1].material.needsUpdate=true;

            if(moveSlowly)
            {   
                objects[i].position.x += objects[i].directionX;
                objects[i].position.y += objects[i].directionY;
                objects[i].position.z += objects[i].directionZ;
               
            }

        }
    };

    Webgl.prototype.onMouseMove=function(event){

        // console.log("camera x",this.camera.position );
   
        event.preventDefault();
        var mouseX = ( event.clientX / window.innerWidth ) * 2 - 1;
        var mouseY = -( event.clientY / window.innerHeight ) * 2 + 1;

        var vector = new THREE.Vector3( mouseX, mouseY, this.camera.near ).unproject( this.camera );

        var raycaster = new THREE.Raycaster( this.camera.position, vector.sub( this.camera.position ).normalize() );
        var intersections = raycaster.intersectObjects(sphereMeshes);


        if ( intersections.length > 0 ) {
            // console.log("vevevfe",intersections[ 0 ].object.parent.children[1]);
            if ( intersected != intersections[ 0 ].object.parent.children[1] ) {
                if ( intersected ) TweenLite.to(intersected.scale, 0.5, {x:1,y:1,z:1});

                    // TweenLite.to(intersects[ 0 ].object.scale, 0.3, {x:2,y:2,z:2,ease:Circ.easeInOut});
                intersected = intersections[ 0 ].object.parent.children[1];
                TweenLite.to(intersected.scale, 0.5, {x:3,y:3,z:3});

            }
            document.body.style.cursor = 'pointer';


        }
        else if ( intersected ) {

            TweenLite.to(intersected.scale, 0.5, {x:1,y:1,z:1});
            intersected = null;
            document.body.style.cursor = 'auto';

        }

    };

    Webgl.prototype.onMouseClick=function(event){

   
        event.preventDefault();
        var mouseX = ( event.clientX / window.innerWidth ) * 2 - 1;
        var mouseY = -( event.clientY / window.innerHeight ) * 2 + 1;

        var vector = new THREE.Vector3( mouseX, mouseY, this.camera.near ).unproject( this.camera );

        var raycaster = new THREE.Raycaster( this.camera.position, vector.sub( this.camera.position ).normalize() );
        var intersections = raycaster.intersectObjects(sphereMeshes);


        if ( intersections.length > 0 ) {
           
            this.explode();

        }

    };


    return Webgl;

})();