var webgl, shape, geometry;

var colors1 = ['#cccccc','#BD1550','#E97F02','#F8CA00','#8A9B0F'];
var colors2 = ['#554236','#F77825','#D3CE3D','#F1EFA5','#60B99A'];
var colors3 = ['#058789','#503D2E','#D54B1A','#E3A72F','#F0ECC9'];
var colors4 = ['#666666','#C94D65','#E7C049','#92B35A','#1F6764'];
var colors5 = ['#3F2C26','#DD423E','#A2A384','#EAC388','#C5AD4B'];
var arraysColors = [colors1, colors2, colors3, colors4, colors5];



$(document).ready(init);

function init(){
    webgl = new Webgl(window.innerWidth, window.innerHeight);

    $(window).on('resize', resizeHandler);
    $(window).on('mousemove', mouseMove);
    $(window).on('click', mouseClick);

    var snd = document.getElementById("mySound");
    snd.volume = 0.3;


    helix = false;



    $('.choice').on('click', function(){
		webgl.toggleColor($(this).attr('id'));
		$('.choice').removeClass('selected');
		$(this).addClass('selected');
	});

    for(i=0; i<arraysColors.length; i++)
    {	
    	arrayColor = arraysColors[i];
    	$("#colors"+(i+1)+" div").each(function(index){
	   		$(this).css('background-color', arrayColor[index]);
   	 	});
    }

    $('#helix').on('click',function(){

    	if(!$(this).hasClass('selected'))
		{
			webgl.displayHelix();
			// webgl.toggleShapes();
			$('.shapes img').removeClass('selected');
			$(this).addClass('selected');
			
		}

		fadeInOutSound(3)

	});

	$('#sphere').on('click',function(){
		if(!$(this).hasClass('selected'))
		{
			webgl.displaySphere(Back.easeInOut, 2.5, false);
			$('.shapes img').removeClass('selected');
			$(this).addClass('selected');
			


		}

		fadeInOutSound(2.5)

	});
	$('#rings').on('click',function(){

    	if(!$(this).hasClass('selected'))
		{
			webgl.displayRings();
			$('.shapes img').removeClass('selected');
			$(this).addClass('selected');
			
		
		}

		fadeInOutSound(3);

	});

	$('#rose').on('click',function(){
		if(!$(this).hasClass('selected'))
		{
			webgl.displayRose();
			$('.shapes img').removeClass('selected');
			$(this).addClass('selected');
			
		}

		fadeInOutSound(3);

	});

	$('.geometry img').on('click', function(){

		if(!$(this).hasClass('selected'))
		{
			webgl.toggleGeometry(parseInt($(this).attr('data-index')));
			$('.geometry img').removeClass('selected');
			$(this).addClass('selected');
			
		}
		fadeInOutSound(3);

	});

	$("#soundoff").on('click', function(){

		var snd = document.getElementById("mySound");
		snd.pause();
		$(this).css('display','none');
		$("#soundon").css('display','inline');
	});

	$("#soundon").on('click', function(){
		
		var snd = document.getElementById("mySound");
		snd.play();
		$(this).css('display','none');
		$("#soundoff").css('display','inline');
	});



}

function fadeInOutSound(time)
{
	$("#mySound").animate({volume: 1}, time*1000/2, function(){
		$(this).animate({volume: 0.2}, time*1000/2);
	});
}

function resizeHandler() {
    webgl.resize(window.innerWidth, window.innerHeight);
}

function mouseMove(e) {
    webgl.onMouseMove(e);
}

function mouseClick(e) {
    webgl.onMouseClick(e);
}

