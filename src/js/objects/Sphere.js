var Sphere = (function(){

    function Sphere(radius){
        THREE.Object3D.call(this);

        var geometry = new THREE.SphereGeometry(radius);
        var material = new THREE.MeshBasicMaterial({transparent:true,opacity:0});
        this.mesh = new THREE.Mesh(geometry, material);
        this.add(this.mesh);
    }

    Sphere.prototype = new THREE.Object3D;
    Sphere.prototype.constructor = Sphere;

    // Sphere.prototype.update = function() {
    //     this.mesh.rotation.y += 0.01;
    // };

    return Sphere;
})();