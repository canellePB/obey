var tubeRadius = 1;
var radialSegments = 10;
var segments =64;

var Hexagone = (function(){

    function Hexagone(radius,material, tubularSegments){

        THREE.Mesh.call(this,geometry,material);
        var geometry = new THREE.TorusGeometry( radius, tubeRadius, radialSegments, tubularSegments );
        this.geometry=geometry;
        this.material=material;


    }

    Hexagone.prototype = new THREE.Mesh;
    Hexagone.prototype.constructor = Hexagone;



    return Hexagone;
})();
